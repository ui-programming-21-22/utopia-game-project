if(typeof(Storage) !== "undefined") 
{
    // Local storage is available on your browser
    if (localStorage.getItem('musicVolume') != null)
    {
        musicVolume = parseFloat(localStorage.getItem('musicVolume'));
        barIndex = parseFloat(localStorage.getItem('musicIndex'));
        soundEffectVolume = parseFloat(localStorage.getItem('soundVolume'));
        bar2Index = parseFloat(localStorage.getItem('soundIndex'));
        hiScore = parseInt(localStorage.getItem('highScore'));
    }
} 
else 
{
    console.log("Local storage is not supported.");
}


function validateForm()
{
    localStorage.setItem("musicVolume", musicVolume);
    localStorage.setItem("musicIndex", barIndex);
    localStorage.setItem("soundVolume", soundEffectVolume);
    localStorage.setItem("soundIndex", bar2Index);
    localStorage.setItem("highScore", hiScore);
}

const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

const myScreenHeight = 500;
const myScreenWidth = 1100;
let screenHeight = canvas.scrollHeight;
let screenWidth = canvas.scrollWidth;
let scaleHeight = screenHeight / myScreenHeight;
let scaleWidth = screenWidth / myScreenWidth;
let mouseScaleX = myScreenHeight / screenHeight;
let mouseScaleY = myScreenWidth / screenWidth;

//console.log("Width: " +myScreenWidth);
//console.log("Height: " +screenHeight);
//console.log("Scale Height: " +scaleHeight);
//console.log("Scale Weidth: " +scaleWidth);

let shipTexture = new Image();
shipTexture.src = "./Assets/ship.png";
let ballTexture = new Image();
ballTexture.src = "./Assets/ball.png";
let blueBlock = new Image();
blueBlock.src = "./Assets/blue.png";
let greenBlock = new Image();
greenBlock.src = "./Assets/green.png";
let heartTexture = new Image();
heartTexture.src = "./Assets/heart.png";
let backgroundTexure = new Image();
backgroundTexure.src = "./Assets/background.png";
let leftArrowTexture = new Image();
leftArrowTexture.src = "./Assets/left.png";
let rightArrowTexture = new Image();
rightArrowTexture.src = "./Assets/right.png";

// Menu
let menuImage = new Image();
menuImage.src = "./Assets/Menu/menu.png";
let resumeImage = new Image();
resumeImage.src = "./Assets/Menu/resume.png";
let muteImage = new Image();
muteImage.src = "./Assets/Menu/mute.png";
let musicImage = new Image();
musicImage.src = "./Assets/Menu/music.png";
let soundsImage = new Image();
soundsImage.src = "./Assets/Menu/sounds.png";
let plusImage = new Image();
plusImage.src = "./Assets/Menu/plus.png";
let minusImage = new Image();
minusImage.src = "./Assets/Menu/minus.png";
let barImage = new Image();
barImage.src = "./Assets/Menu/bar.png";

// Touch screen
let touchBool = false;

// Sound Assets
let hitWall = new Audio('./Assets/hitWall.wav');
let hitBlock = new Audio('./Assets/hitBlock.wav');
let hitHeart = new Audio('./Assets/hitHeart.wav');
let music = new Audio('./Assets/music.mp3');

let soundEffectVolume = 1.0;
let musicVolume = 1.0;
let soundSet = false;

// Begin Loading
backgroundTexure.onload = gameloop;

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height, scale) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.scale = scale;
}

function Block(spritesheet, x, y, width, height, scale, hits) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.scale = scale;
    this.hits = hits;
}

//Mouse position
function MousePosition(x, y)
{
    this.x = x;
    this.y = y;
}
let mousePosition = new MousePosition(0,0);
let mouseDownBool = false;

// Breakable Blocks
let blockArray = [];

// Level array used to make the level
let level = [  
    2,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,2,
    0,0,0,0,0,1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,1,0,0,0,0,0,0,
    0,0,0,0,0,1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,1,0,0,0,0,0,0,
    1,1,0,0,1,1,1,1,2,1,2,1,2,2,2,1,2,1,2,1,1,1,1,1,0,0,1,1,
    2,2,0,0,0,0,0,0,0,0,0,0,1,2,1,0,0,0,0,0,0,0,0,0,0,0,2,2,
 ];
let levelXOffset = 20;
let levelYOffset = 50;
let MAX_PER_ROW = 28;

// Creates the blocks and put them in an array
for (let index = 0; index < level.length; index++) 
{
    if (level[index] == 1)
    {
        const newBlock = new Block(greenBlock, levelXOffset, levelYOffset, 75, 75, 0.5, 1);
        blockArray.push(newBlock);
    }
    else if (level[index] == 2)
    {
       const newBlock = new Block(blueBlock, levelXOffset, levelYOffset, 75, 75, 0.5, 2);
       blockArray.push(newBlock);
    }
    else
    {
        const newBlock = new Block(greenBlock, levelXOffset, levelYOffset, 75, 75, 0.5, 0);
        blockArray.push(newBlock);
    }

    levelXOffset += 38

    if (index % MAX_PER_ROW == MAX_PER_ROW - 1)
    {
        levelYOffset += 39;
        // Reset X to Original
        levelXOffset = 20;
    }
}

// Default Player
let ship = new GameObject(shipTexture, 450, 460, 509, 91, 0.4);
let shipIndex = 0;
const speed = 3;
let score = 0;
let hiScore = 0;
let gameOver = false;

// Enemy Heart
let heart = new Block (heartTexture, 490, 90, 348, 311,  0.25, 3);
let heartIndex = 0;
blockArray.push(heart);
level.push(5);

// Ball
let ball = new GameObject(ballTexture, 535, 430, 112, 108, 0.3);
let ballSpeed = 3;
let stuck = true; // if the ball is stuck to the ship
let ready = true; // if the player isnt touching the buttons
let resumeReady = false; // fixes error with ball going after pressing resume
let directionX = 0; // direction for ball movement
let directionY = 0;
let ballsUsed = 0;
let ballsCaught = 0;

// Background
let background = new GameObject(backgroundTexure, 0, 0, 829, 471, 1.4);
let background2 = new GameObject(backgroundTexure, 0, -background.height * background.scale, 829, 471, 1.4);

// Buttons
let leftArrow = new GameObject(leftArrowTexture, 0, 360, 345, 353, 0.4);
let rightArrow = new GameObject(rightArrowTexture, 960, 360, 345, 353, 0.4);
let clickBool = false;

// Menu System                                  x    y   wid  hei  scale
let menu = new GameObject(menuImage,            375, 75, 350, 400,  1);
let resume = new GameObject(resumeImage,        460, 125, 184, 40,  1);
let mute = new GameObject(muteImage,            490, 175, 126, 42,  1);
let musicSymbol = new GameObject(musicImage,    530, 230, 42,  39,  1);
let soundsSymbol = new GameObject(soundsImage,  530, 330, 42,  39,  1);
let minus = new GameObject(minusImage,          390, 280, 42,  39,  1);
let plus = new GameObject(plusImage,            670, 280, 42,  39,  1);
let minus2 = new GameObject(minusImage,         390, 380, 42,  39,  1);
let plus2 = new GameObject(plusImage,           670, 380, 42,  39,  1);
let bar = new GameObject(barImage,              430, 285, 297, 40.8,  0.8);
let bar2 = new GameObject(barImage,             430, 385, 297, 40.8,  0.8);

let menuBool = true;
let barIndex = 0;
let bar2Index = 0;

// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

function process_touchstart(e) 
{
    if (e != undefined)
    {
        if (e.type === "touchstart")
        {
            touchBool = true;
            clickBool = true;
            mouseDownBool = true;

            var rect = canvas.getBoundingClientRect();
            mousePosition.x = (parseInt(e.changedTouches[0].clientX) - rect.left) * mouseScaleX;
            mousePosition.y = (parseInt(e.changedTouches[0].clientY) - rect.top) * mouseScaleY;
        }
        if (e.type === "touchmove")
        {
            var rect = canvas.getBoundingClientRect();
            mousePosition.x = (parseInt(e.changedTouches[0].clientX) - rect.left) * mouseScaleX;
            mousePosition.y = (parseInt(e.changedTouches[0].clientY) - rect.top) * mouseScaleY;
        }
        if (e.type === "touchend")
        {
            mouseDownBool = false;

            //Ball movement
            if (stuck != false && ready && resumeReady && !gameOver)
            {
                mouseDownBool = false;
                stuck = false;
                calculateDirection();
            }
        }
    }
}

function input(event) {
    if (!touchBool)
    {    
        // Clicks
        if (event.type === "click") 
        {
            clickBool = true;
            touchBool = false;

            //Ball movement
            if (stuck != false && ready && resumeReady && !gameOver)
            {
                stuck = false;
                calculateDirection();
            }
            // Debug
            //console.log(blockArray[0].x);
            //console.log(mousePosition.x);
        }
        // Click Down
        if (event.type === "mousedown") 
        {
            mouseDownBool = true;
        }
        // Click Release
        if (event.type === "mouseup") 
        {
            mouseDownBool = false;
        }
        // Key Downs
        if (event.type === "keydown") 
        {
            switch (event.keyCode) 
            {
                case 65: // A - Left
                    gamerInput = new GamerInput("Left");
                    break;
                case 68: // D - Right 
                    gamerInput = new GamerInput("Right");
                    break;
                case 27: // Esc
                    menuBool = !menuBool;
                    resumeReady = !resumeReady;
                    music.play();
                    validateForm();
                    break;
            }
        }
        // Key Downs
        if (event.type === "keyup") 
        {
            switch (event.keyCode) 
            {
                case 65: // A - Left
                    gamerInput = new GamerInput("None");
                    break;
                case 68: // D - Right 
                    gamerInput = new GamerInput("None");
                    break;
            }
        }
    }
}

// Gets mouse position
function getMousePosition()
{
    canvas.style.cursor = 'default';

    if (clickBool)
    {
        setTimeout(clickOff, 5);
    }
    onmousemove = function(e){
        // Get Mouse coords
        var rect = canvas.getBoundingClientRect();
        mousePosition.x = (e.clientX - rect.left) * mouseScaleX;
        mousePosition.y = (e.clientY - rect.top) * mouseScaleY;
    }
}

// Aim line draw
function drawAimLine()
{
    if (stuck && mouseDownBool)
    {
        let ballX = ball.x + ((ball.width * ball.scale) / 2);
        let ballY = ball.y + ((ball.height * ball.scale) / 2);

        context.strokeStyle = 'grey';
        context.lineWidth = 1;

        context.beginPath();
        context.setLineDash([5, 15]);
        context.moveTo(mousePosition.x, mousePosition.y);
        context.lineTo(ballX, ballY);
        context.stroke();
    }
}

function update() 
{
    getMousePosition();
    if (!menuBool)
    {
        process_touchstart();
        inputs();
        moveBall();
        getBounceDirection();
        collisionCheck();
        moveBackground();
        movementButtons();
    }
    menuButtons();
    setVolumes();
    windowResizer();
}

// Check Inputs
function inputs()
{
  if (gamerInput.action === "Left") 
  {
      if (ship.x > 0)
      {
          ship.x -= speed;
          if (stuck == true)
          {
              ball.x -= speed;
          }
      }     
  } 
  else if (gamerInput.action === "Right") 
  {
      if (ship.x + (ship.width * ship.scale) < canvas.width)
      {
          ship.x += speed; 
          if (stuck == true)
          {
              ball.x += speed;
          }
      }
  }  
}

// Score System
function scoreTracker()
{
    context.font = "26px Arial";
    context.fillStyle = "white";
    context.fillText("Score: " + score, 5, 30);
    context.fillText("Balls Used: " + ballsUsed, 930, 30);
    if (menuBool)
    {
        context.fillText("Hi-Score: " + hiScore, 490, 30);
    }
    if (gameOver && stuck)
    {
        let scoreDeduction = ballsUsed * -50;
        let caughtDeduction = ballsCaught * 25;
        let finalScore = scoreDeduction + score + caughtDeduction;

        if (score > hiScore)
        {
            hiScore = finalScore;
        }

        context.font = "bold 30px Arial";
        context.fillText("Balls lost: " +ballsUsed+ " x -50 = " + scoreDeduction, 400, 275);
        context.fillText("Balls caught: " +ballsCaught+ " x +25 = " + caughtDeduction, 380, 325);
        context.fillText("Final Score: " + finalScore, 440, 375);
    }
}
  
// Event Listeners
canvas.addEventListener('touchstart', process_touchstart);
canvas.addEventListener('touchmove', process_touchstart);
canvas.addEventListener('touchend', process_touchstart);
window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
window.addEventListener('click', input);
window.addEventListener('mouseup', input);
window.addEventListener('mousedown', input);

// Draw Function
function draw() 
{
    // Background
    context.drawImage(background.spritesheet, 0, 0, background.width, background.height, background.x , background.y, background.width * background.scale, background.height * background.scale);
    context.drawImage(background2.spritesheet, 0, 0, background2.width, background2.height, background2.x , background2.y, background2.width * background2.scale, background2.height * background2.scale);
    drawAimLine();
    // Ship
    context.drawImage(ship.spritesheet, shipIndex * ship.width, 0, ship.width, ship.height, ship.x , ship.y, ship.width * ship.scale, ship.height * ship.scale);
    // Ball
    context.drawImage(ball.spritesheet, 0, 0, ball.width, ball.height, ball.x , ball.y, ball.width * ball.scale, ball.height * ball.scale);
    
    // Block drawing
    for (let index = 0; index < level.length; index++) 
    {
        if (blockArray[index].hits > 0)
        {
            let heartAnimationIndex = 0;

            if (blockArray[index].scale == 0.25)
            {
                heartAnimationIndex = heartIndex * blockArray[index].width;
            }
            context.drawImage(
                blockArray[index].spritesheet, heartAnimationIndex, 0, 
                blockArray[index].width, 
                blockArray[index].height,
                blockArray[index].x,
                blockArray[index].y,
                blockArray[index].width * blockArray[index].scale,
                blockArray[index].height * blockArray[index].scale)
       }
    }
     // Score
     scoreTracker();
     context.drawImage(leftArrow.spritesheet, 0, 0, leftArrow.width, leftArrow.height, leftArrow.x , leftArrow.y, leftArrow.width * leftArrow.scale, leftArrow.height * leftArrow.scale);
     context.drawImage(rightArrow.spritesheet, 0, 0, rightArrow.width, rightArrow.height, rightArrow.x , rightArrow.y, rightArrow.width * rightArrow.scale, rightArrow.height * rightArrow.scale);

     // menu
     if (menuBool)
     {
        context.drawImage(menu.spritesheet, 0, 0, menu.width, menu.height, menu.x , menu.y, menu.width * menu.scale, menu.height * menu.scale);
        context.drawImage(resume.spritesheet, 0, 0, resume.width, resume.height, resume.x , resume.y, resume.width * resume.scale, resume.height * resume.scale);
        context.drawImage(mute.spritesheet, 0, 0, mute.width, mute.height, mute.x , mute.y, mute.width * mute.scale, mute.height * mute.scale);
        context.drawImage(soundsSymbol.spritesheet, 0, 0, soundsSymbol.width, soundsSymbol.height, soundsSymbol.x , soundsSymbol.y, soundsSymbol.width * soundsSymbol.scale, soundsSymbol.height * soundsSymbol.scale);
        context.drawImage(musicSymbol.spritesheet, 0, 0, musicSymbol.width, musicSymbol.height, musicSymbol.x , musicSymbol.y, musicSymbol.width * musicSymbol.scale, musicSymbol.height * musicSymbol.scale);
        context.drawImage(minus.spritesheet, 0, 0, minus.width, minus.height, minus.x , minus.y, minus.width * minus.scale, minus.height * minus.scale);
        context.drawImage(plus.spritesheet, 0, 0, plus.width, plus.height, plus.x , plus.y, plus.width * plus.scale, plus.height * plus.scale);
        context.drawImage(minus2.spritesheet, 0, 0, minus2.width, minus2.height, minus2.x , minus2.y, minus2.width * minus2.scale, minus2.height * minus2.scale);
        context.drawImage(plus2.spritesheet, 0, 0, plus2.width, plus2.height, plus2.x , plus2.y, plus2.width * plus2.scale, plus2.height * plus2.scale);
        context.drawImage(bar.spritesheet, 0, barIndex * 41.5, bar.width, bar.height, bar.x , bar.y, bar.width * bar.scale, bar.height * bar.scale);
        context.drawImage(bar2.spritesheet, 0, bar2Index * 41.5, bar2.width, bar2.height, bar2.x , bar2.y, bar2.width * bar2.scale, bar2.height * bar2.scale);
     }
}

function gameloop()
{
    context.clearRect(0, 0, canvas.width, canvas.height);

    update();
    draw();
   
    window.requestAnimationFrame(gameloop);
}

function moveBall()
{
    if (stuck == false)
    {
        ball.x += directionX * ballSpeed;
        ball.y += directionY * ballSpeed;
    }
}

// Collision checking
function collisionCheck()
{
    let ballX = ball.x + (ball.width * ball.scale) * 0.5;
    let ballY = ball.y + (ball.height * ball.scale) * 0.5;
    let radius = (ball.width * ball.scale) * 0.3;

    // Bounds collision check
    if (ballX > canvas.width - radius || ballX < 0 + radius||
       ballY > canvas.height - radius || ballY < 0 + radius)
    {
        // Right Side
       if (ballX > canvas.width - radius)
       {
            directionX = -1 * directionX;
            hitWall.play();
       }
        // Left Side
       if (ballX < 0 + radius)
       {
            directionX = -1 * directionX;
            hitWall.play();
       }
        // Bottom Side
       if (ballY > canvas.height + radius * 3)
        {
            // Reset Ball
            stuck = true;
            ball.x = ship.x + 85;
            ball.y = 430;
            ballsUsed++;
       }
        // Top Side
       if (ballY < 0 + radius)
       {
            directionY = -1 * directionY;
            hitWall.play();
       }
    }

    // Ship and Ball collision to catch the ball
    if (ballX > ship.x + 10 && ballX < ship.x + (ship.width * ship.scale) - 10 &&
        ballY > ship.y && ballY < ship.y + (ship.height * ship.scale))
    {
        shipIndex = 1;
        setTimeout(highlightShip,100);
        ballsCaught++;
        ballsUsed++;
        // Reset Ball
        stuck = true;
        ball.x = ship.x + 85;
        ball.y = 430;
    }
}

// Calculates direction between mouse and ball
function calculateDirection()
{
    let dirX = mousePosition.x - (ball.x + ((ball.width * ball.scale) / 2));
    let dirY = mousePosition.y - (ball.y + ((ball.height * ball.scale) / 2));

	let vectorLength = Math.sqrt(dirX * dirX + dirY * dirY);

	directionX = dirX / vectorLength;
    directionY = dirY / vectorLength;
}

// Bounces ball off of surfaces using intersection 
function getBounceDirection()
{
    for (let index = 0; index < level.length; index++) {

        if (blockArray[index].hits > 0)
        {
            const brickTop = blockArray[index].y - blockArray[index].height *  blockArray[index].scale;
            const brickBottom = blockArray[index].y + blockArray[index].height *  blockArray[index].scale;
            const brickLeft = blockArray[index].x - blockArray[index].width *  blockArray[index].scale;
            const brickRight = blockArray[index].x + blockArray[index].width *  blockArray[index].scale;

            let offSetLeft = 0;
            let offSetRight = 0;
            
            // Gives different offsets when colliding with heart 
            if (level[index] == 5)
            {
                offSetLeft = -50;
                offSetRight = -20;
            }

            const ballTop = ball.y - offSetRight;
            const ballBottom = ball.y + offSetLeft;
            const ballLeft = ball.x - offSetRight;
            const ballRight = ball.x + offSetLeft;

            if (ballTop < brickBottom && ballLeft < brickRight && ballBottom > brickTop && ballRight > brickLeft)
            {
                // Damages Block
                blockArray[index].hits -= 1;
                // Blocks that get destroyed give score
                if (blockArray[index].hits == 0)
                {
                    score += 25;
                }
                // Heart that get destroyed give score
                if (blockArray[index].hits == 0 && level[index] == 5)
                {
                    gameOver = true;
                    score += 250;
                }
                
                // Highlights heart when hit
                if (level[index] == 5)
                {
                    heartIndex = 1;
                    setTimeout(highlightHeart, 100);
                    hitHeart.play();
                }

                const X1 = Math.abs(ballRight - brickLeft);
                const X2 = Math.abs(ballLeft - brickRight);
                const Y1 = Math.abs(ballTop - brickBottom);
                const Y2 = Math.abs(ballBottom - brickTop);

                // Bottom side collision
                if (directionY < 0 && Y1 < Y2 && Y1 < X1 && Y1 < X2)
                {
                    directionY = -1 * directionY;
                    hitBlock.play();
                }
                // Top side collision
                if (directionY > 0 && Y2 < Y1 && Y2 < X1 && Y2 < X2)
                {
                    directionY = -1 * directionY;
                    hitBlock.play();
                }
                // Left side collision
                if (directionX > 0 && X1 < X2 && X1 < Y1 && X1 < Y2)
                {
                    directionX = -1 * directionX;
                    hitBlock.play();
                }
                // Right side collision
                if (directionX < 0 && X2 < X1 && X2 < Y1 && X2 < Y2)
                {
                    directionX = -1 * directionX;
                    hitBlock.play();
                }
                break;
            }
        }
    }
}

function moveBackground()
{
    const backgroundSpeed = 1;
    const backgroundRestartPosition = background.height * background.scale;

    background.y += backgroundSpeed;
    background2.y += backgroundSpeed;

    if (background.y > canvas.height)
    {
       background.y = background2.y - backgroundRestartPosition;
    }
    if (background2.y > canvas.height)
    {
       background2.y = background.y - backgroundRestartPosition;
    }
}

function setVolumes()
{
    if (menuBool)
    {
        hitWall.volume = 0.8 * soundEffectVolume;
        hitBlock.volume = 0.2 * soundEffectVolume;
        hitHeart.volume = 0.9 * soundEffectVolume;
        music.volume = 0.7 * musicVolume;
    }
}

function movementButtons()
{
    // Left Arrow Collision
    if (mousePosition.x > leftArrow.x && mousePosition.x < leftArrow.x + (leftArrow.width * leftArrow.scale) &&
        mousePosition.y > leftArrow.y && mousePosition.y < leftArrow.y + (leftArrow.height * leftArrow.scale))
    {
        if (mouseDownBool)
        {
            gamerInput = new GamerInput("Left");
        }
        else
        {
            gamerInput = new GamerInput("None");
        }
        canvas.style.cursor = 'pointer';
        ready = false;
        return;
    }

    // Right Arrow Collision
    if (mousePosition.x > rightArrow.x && mousePosition.x < (rightArrow.x + (rightArrow.width * rightArrow.scale)) &&
        mousePosition.y > rightArrow.y && mousePosition.y < (rightArrow.y + (rightArrow.height * rightArrow.scale)))
    {
        if (mouseDownBool)
        {
            gamerInput = new GamerInput("Right");
        }
        else
        {
            gamerInput = new GamerInput("None");
        }
        canvas.style.cursor = 'pointer';
        ready = false;
        return;
    }
    else
    {
        canvas.style.cursor = 'default';
        ready = true;
    }
}

function menuButtons()
{
    if (menuBool)
    {
        // Resume
        if (mousePosition.x > resume.x && mousePosition.x < resume.x + (resume.width * resume.scale) &&
            mousePosition.y > resume.y && mousePosition.y < resume.y + (resume.height * resume.scale))
        {
            if (clickBool)
            {
                setTimeout(resumeReadyOn, 20);
                menuBool = false;
                music.play();
                validateForm();
            }
            canvas.style.cursor = 'pointer';
            return;
        }
        // Mute
        else if (mousePosition.x > mute.x && mousePosition.x < mute.x + (mute.width * mute.scale) &&
                 mousePosition.y > mute.y && mousePosition.y < mute.y + (mute.height * mute.scale))
        {
            if (clickBool)
            {
                // Turn off all sound and reduce all bars
                barIndex = 5;
                bar2Index = 5;
                soundEffectVolume = 0;
                musicVolume = 0;
            }
            canvas.style.cursor = 'pointer';
            return;
        }
        // Music Symbol
        else if (mousePosition.x > musicSymbol.x && mousePosition.x < musicSymbol.x + (musicSymbol.width * musicSymbol.scale) &&
                 mousePosition.y > musicSymbol.y && mousePosition.y < musicSymbol.y + (musicSymbol.height * musicSymbol.scale))
        {
            if (clickBool)
            {
                if (barIndex != 5)
                {
                    // Reduce Bar to 0
                    barIndex = 5;

                    // Reduce music to 0
                    musicVolume = 0;
                }
                else 
                {
                    barIndex = 0;
                    musicVolume = 1;
                }
            }
            canvas.style.cursor = 'pointer';
            return;
        }
        // Music minus
        else if (mousePosition.x > minus.x && mousePosition.x < minus.x + (minus.width * minus.scale) &&
                 mousePosition.y > minus.y && mousePosition.y < minus.y + (minus.height * minus.scale))
        {
            if (clickBool)
            {
                // Reduce bar
                barIndex += 1;
                if (barIndex > 5)
                {
                    barIndex = 5;
                }
                // Reduce music
                musicVolume -= 0.2;
                if (musicVolume < 0.0)
                {
                    musicVolume = 0.0;
                }
            }
            canvas.style.cursor = 'pointer';
            return;
        }
        // Music plus
        else if (mousePosition.x > plus.x && mousePosition.x < plus.x + (plus.width * plus.scale) &&
                mousePosition.y > plus.y && mousePosition.y < plus.y + (plus.height * plus.scale))
        {
            if (clickBool)
            {
                // Increase bar
                barIndex -= 1;
                if (barIndex < 0)
                {
                    barIndex = 0;
                }
                // Increase music
                musicVolume += 0.2;
                if (musicVolume > 1.0)
                {
                    musicVolume = 1.0;
                }
            }
            canvas.style.cursor = 'pointer';
            return;
        }
        // Sound Symbol
        else if (mousePosition.x > soundsSymbol.x && mousePosition.x < soundsSymbol.x + (soundsSymbol.width * soundsSymbol.scale) &&
                 mousePosition.y > soundsSymbol.y && mousePosition.y < soundsSymbol.y + (soundsSymbol.height * soundsSymbol.scale))
        {
            // Sound off
            if (clickBool)
            {
                if (bar2Index != 5)
                {
                    // Reduce Bar to 0
                    bar2Index = 5;

                    // Reduce Sound to 0
                    soundEffectVolume = 0;
                }
                else 
                {
                    bar2Index = 0;
                    soundEffectVolume = 1;
                }
            }
            canvas.style.cursor = 'pointer';
            return;
        }
        // Sound minus
        else if (mousePosition.x > minus2.x && mousePosition.x < minus2.x + (minus2.width * minus2.scale) &&
                 mousePosition.y > minus2.y && mousePosition.y < minus2.y + (minus2.height * minus2.scale))
        {
            if (clickBool)
            {
                // Reduce bar
                bar2Index += 1;
                if (bar2Index > 5)
                {
                    bar2Index = 5;
                }
                // Reduce sound
                soundEffectVolume -= 0.2;
                if (soundEffectVolume < 0.0)
                {
                    soundEffectVolume = 0.0;
                } 
                hitHeart.play();
            }
            canvas.style.cursor = 'pointer';
            return;
        }
        // Sound plus
        else if (mousePosition.x > plus2.x && mousePosition.x < plus2.x + (plus.width * plus2.scale) &&
                 mousePosition.y > plus2.y && mousePosition.y < plus2.y + (plus2.height * plus2.scale))
        {
            if (clickBool)
            { 
                // Increase bar
                bar2Index -= 1;
                if (bar2Index < 0)
                {
                    bar2Index = 0;
                } 
                // Increase sound
                soundEffectVolume += 0.2;
                if (soundEffectVolume > 1.0)
                {
                    soundEffectVolume = 1.0;
                }
                hitHeart.play();
            }
            canvas.style.cursor = 'pointer';
            return;
        }
    }
}

function windowResizer()
{
    screenHeight = canvas.scrollHeight;
    screenWidth = canvas.scrollWidth;
    scaleHeight = screenHeight / myScreenHeight;
    scaleWidth = screenWidth / myScreenWidth;
    mouseScaleX = myScreenHeight / screenHeight;
    mouseScaleY = myScreenWidth / screenWidth;
}

function clickOff() {clickBool = false;}
function resumeReadyOn() {resumeReady = true;}
function highlightHeart(){heartIndex = 0;}
function highlightShip(){shipIndex = 0;}